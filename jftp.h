#ifndef _JFTP_H
#define _JFTP_H

#include "Thread.h"
#include "FileSystem.h"
#include "Socket.h"
#include "Arguments.h"
#include "Util.h"
#include "FileSystem.h"
#include "Control.h"

const u16 DefaultPort = 1234;

enum ArgChars {
	ARG_HELP = 'h',
	ARG_PORT = 'p',
	ARG_DEST = 'd',
	ARG_CMDS = 'c',
};

const u32 MaxPacketSize = 2048;
const u32 BufferSize = 1024;
const int MaxConnections = 20;
const u32 RetryCount = 3;
const int TimeoutSeconds = 1;

static inline bool DataWithinTimeout(Socket s) {
	for(u32 i=0; i<RetryCount; i++) {
		if(s.HasData(TimeoutSeconds)) {
			return true;
		}
	}
	return false;
}

const char* const NewTransferMsg = "OK";



#endif

