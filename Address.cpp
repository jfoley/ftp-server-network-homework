#include "Address.h"
#include "Util.h"

//--- network specific
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <cstring> //memcpy
#include <cstdio>

Address::Address(void *blob, size_t len) {
	
	//--- invalid
	if(!blob || !len) {
		return;
	}

	//--- copy
	const u8* byte = (const u8*) blob;
	for(size_t i=0; i<len; i++) {
		data.push_back(byte[i]);
	}
}

bool Address::Valid() const {
	if(data.size() == 0) return false;

	const sockaddr* sa = (const sockaddr*) &data[0];
	switch(sa->sa_family) {
		case AF_INET:
			return data.size() >= sizeof(sockaddr_in);
		case AF_INET6:
			return data.size() >= sizeof(sockaddr_in6);
		default:
			return false;
	}
}

string Address::ToString() const {
	string host = Host();
	string port;
	if(Port())
		port += stringf(":%d", Port());

	return host+port;
}

string Address::Host() const {
	int family = Family();

	switch(family) {
		case AF_INET:
			{
				const sockaddr_in* addr = (sockaddr_in*) &data[0];

				//--- deconstruct word
				const u32 ip_word = ntohl(addr->sin_addr.s_addr);
				const u8 a = (ip_word >> 24) & 0xff;
				const u8 b = (ip_word >> 16) & 0xff;
				const u8 c = (ip_word >> 8)  & 0xff;
				const u8 d = (ip_word)       & 0xff;

				return stringf("%d.%d.%d.%d", a, b, c, d);
			}
			break;
		case AF_INET6:
			{
				string repr;
				repr.resize(INET6_ADDRSTRLEN);
				//--- convert address to string
				inet_ntop(family, &data[0], &repr[0], repr.size());
				return repr;
			}
		default:
			return "";
			break;
	}

}

bool Address::operator==(const Address &rhs) const {
	if(!Valid() || !rhs.Valid()) return false;
	return Family() == rhs.Family() && ToString() == rhs.ToString();
}

int Address::Family() const {
	if(!Valid()) return -1;
	const sockaddr* sa = (const sockaddr*) &data[0];
	return sa->sa_family;
}

u16 Address::Port() const {
	switch(Family()) {
		case AF_INET:
			return ntohs(((const sockaddr_in*) &data[0])->sin_port);
		case AF_INET6:
			return ntohs(((const sockaddr_in6*) &data[0])->sin6_port);
		default:
			return 0;
	}
}

Address Address::Resolve(const string& host, u16 port) {
	addrinfo *results = 0;
	//--- gen request parameters
	{
		const string service = stringf("%d", port);
		const char* service_ptr = (port == 0) ? 0 : service.c_str();
		const char* host_ptr = host.c_str();

		addrinfo hints;
		memset(&hints, 0, sizeof(hints));
		hints.ai_family = AF_UNSPEC; //AF_INET; //--- prefer IPv4 addresses
		
		//--- allow localhost
		if(host == "localhost" || host == "")
			hints.ai_flags = AI_PASSIVE;

		int status = getaddrinfo(host_ptr, service_ptr, &hints, &results);

		if(status != 0) {
			std::cout << "getaddrinfo: " << gai_strerror(status) << "\n";
			results = 0;
			return Address();
		}
	}

	Address result;
	bool found = false;
	//--- process results
	for(addrinfo *cur = results; cur != 0; cur = cur->ai_next ) {
		//if(cur->ai_family != AF_INET) continue;
		//show(Address(cur->ai_addr,cur->ai_addrlen));
		
		if(!found) {
			found = true;
			result = Address(cur->ai_addr, cur->ai_addrlen);
		}
	}


	//--- free linked list from getaddrinfo
	freeaddrinfo(results);
	if(found) {
		return result;
	}

	return Address();
}

