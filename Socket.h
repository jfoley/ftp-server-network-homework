#ifndef _SOCKET_H
#define _SOCKET_H

#include "Address.h"

enum SocketType {
	ErrorSocket=0,
	StreamSocket,
	DatagramSocket,
};

class Socket {
	public:
		//--- two methods of setting up invalid sockets
		Socket();
		Socket(const Socket &rhs);

		//--- a socket will clean up itself if ShouldClose() is set to true
		~Socket() { if(autoclose) { Close(); } }
		void ShouldClose(bool arg=true) { autoclose = arg; }


		bool Valid() const;
		operator bool() const { return Valid(); }

		//--- makes sense only after Bind or Connect
		Address GetAddress() const;

		//--- check for data for up to timeout seconds
		bool HasData(int timeout=0);
		
		//--- how to open stream sockets
		bool OpenStream();
		bool OpenStreamServer (u16 port, int connections);
		bool OpenStreamClient (const Address &target);
		
		//--- operations on stream sockets
		bool Send (const string& msg) { return Send(&msg[0], msg.size()); }
		bool Send (const void *msg, size_t len);
		int  Recv (void *msg, size_t len);
		string RecvAll();
		string RecvUntil(char marker);
		
		//--- how to open datagram sockets
		bool OpenDatagram (u16 port=AnyPort);
		//--- operations on datagram sockets
		bool SendTo       (const Address &dest, const void *msg, size_t len);
		int  RecvFrom     (Address *src, void *msg, size_t len);
		
		void Close();

		Socket Accept(Address *client=0);

		bool Bind(u16 port);
		bool Connect(const Address &target);
		bool Listen(int connections);
		bool SetBlocking(bool shouldBlock);


		friend ostream& operator<<(ostream &os, const Socket& s) { return os << s.handle; }

	private:
		Socket(int, SocketType);

		bool autoclose;
		volatile int handle;
		SocketType type;
};


#endif

