#ifndef __ADDRESS_H
#define __ADDRESS_H

#include "types.h"

const u16 AnyPort = 0;

class Address {
	public:
		/// use getaddrinfo to find host
		static Address Resolve(const string &host, u16 port=AnyPort);
		/// from binary blob, default is invalid address
		Address(void* data=0, size_t len=0);
		/// Determine whether the address is valid or not
		bool Valid() const;
		operator bool() const { return Valid(); }
	
		/// if valid else ""
		string Host() const;
		/// if valid else zero
		u16 Port() const;
		/// if valid else -1
		int Family() const;

		/// For sanity checking
		string ToString() const;
		//operator string() const { return ToString(); }

		bool operator==(const Address &rhs) const;

		const void* Ptr() const { return &data[0]; }
		size_t Size() const { return data.size(); }

		//--- just for C++ fun
		friend ostream& operator<<(ostream& out, const Address &addr) { return out << addr.ToString(); }
		
	private:
		/// hide struct sockaddr & implementation details behind an array of bytes
		vector<u8> data;
};

#endif


