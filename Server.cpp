#include "Server.h"
#include "jftp.h"

#include "ClientHandler.h"
#include "Transfer.h"

void Server::NewClient(Socket s) {
	clients.Collect();

	Thread *thread = new ClientHandler(this, s);
	thread->Start();
	
	clients.Insert(thread);
}

Socket Server::SpawnTransfer(Socket client) {
	Socket offer;
	offer.OpenStreamServer(AnyPort, MaxConnections);
	offer.ShouldClose();

	client.Send(stringf("%s %u", NewTransferMsg, offer.GetAddress().Port()));
	
	return offer.Accept();
}

bool Server::TryLockFile(const string &fileName) {
	ScopeLock lock(mutex);
	
	//--- if it already exists, fail
	if(lockedFiles.find(fileName) != lockedFiles.end()) {
		return false;
	}
	//--- if it doesn't add it
	lockedFiles.insert(fileName);

	return true;
}

void Server::DoneFile(const string &fileName) {
	ScopeLock lock(mutex);
	lockedFiles.erase(fileName);
}


