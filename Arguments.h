#ifndef _ARGUMENTS_H
#define _ARGUMENTS_H

#include "types.h"
#include <map>
using std::map;

class Arguments {
	public:
		Arguments(string name);
		//Arguments(string name) : parsed(false), progName(name) { }
		void DefineFlag( char c, string desc);
		void DefineOption(char c, string desc);
		void DefineParam(char c, string desc);

		bool Parse(int argc, char **argv);

		bool   GetFlag (char c) const;
		string GetParam(char c) const;
		string GetOption(char c, string fallback="") const;

		void Print() const;

	private:
		void Errorf(const char *fmt, ...);

		enum ArgType {
			Flag,     // no argument, present or not
			Option,   // takes argument
			Parameter // required
		};

		struct ArgDef {
			char c;
			string desc;
			ArgType type;
		};

		bool parsed;
		string progName;
		vector<ArgDef> argdefs;
		map<char, string> results;
};

#endif

