#ifndef _UTIL_H
#define _UTIL_H

#include "types.h"

string stringf         (const char * fmt, ...);

vector<string> split   (const string &src, const char delimiter);
string         join    (const vector<string> &src, const char delimiter);

string splitAfter      (const string &src, const char delimiter);
string splitBefore     (const string &src, const char delimiter);
string splitAfterLast  (const string &src, const char delimiter);
string splitBeforeLast (const string &src, const char delimiter);

bool   contains        (const string &haystack, const char needle);
string toLower         (const string &);
bool   isSpaces        (const string &);
string compactSpaces   (const string &);

char   reverseElement  (const string &src, size_t i);
bool   startsWith      (const string &src, const string &prefix);
bool   endsWith        (const string &src, const string &suffix);

int    toInt           (const string &n, int fallback);

#endif

