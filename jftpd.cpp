#include "jftp.h"
#include "Server.h"
#include "ClientHandler.h"
#include "Transfer.h"

Arguments do_args(int argc, char **argv) {
	Arguments args(argv[0]);
	args.DefineFlag (ARG_HELP, "Show this help.");
	args.DefineOption(ARG_PORT, "Port number to listen to requests.");

	if(!args.Parse(argc, argv)) {
		args.Print();
		exit(-1);
	}
	if(args.GetFlag(ARG_HELP)) {
		args.Print();
		exit(0);
	}

	return args;
}

int main(int argc, char **argv) {
	Arguments args = do_args(argc, argv);

	u16 port = toInt(args.GetParam(ARG_PORT), DefaultPort);

	Socket service;
	service.ShouldClose();
	service.OpenStreamServer(port, MaxConnections);

	Server server;

	cout << "Listening on port " << port << "\n";

	while(service) {
		Socket client =	service.Accept();

		if(!client)
			continue;

		server.NewClient(client);
	}

	return 0;
}

