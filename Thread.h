#ifndef _THREAD_H
#define _THREAD_H

#include <pthread.h>

class Thread {
	public:
		virtual ~Thread() { Stop(); }
		void Start();
		void Stop();
		virtual void Process() { }

		volatile bool Active() const { return running; }
		volatile bool running;
	private:
		pthread_t self;
};



#endif

