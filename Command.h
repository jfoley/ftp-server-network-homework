#ifndef __COMMAND_H
#define __COMMAND_H

#include "types.h"

namespace Cmd {
	const string Help = "?";
	const string Quit = "quit";
	const string Get = "get";
	const string Put = "put";
	const string List = "ls";
	const string ChangeDir = "cd";
	const string CurrentDir = "pwd";
};


class Command {
	public:
		Command(string input);

		const string& Verb() const { return verb; }
		const string& Noun() const { return noun; }

		//--- to bool
		bool Valid() const { return verb != ""; }
		operator bool() const { return Valid(); }

		//--- tostring
		string ToString() const { return verb + ' ' + noun + '\n'; }
		operator string() const { return ToString(); }
		friend ostream& operator<<(ostream &out, const Command& cmd) { return out << cmd.ToString(); }

		//--- comparison
		bool operator==(const string &other) const;

	private:
		string verb;
		string noun;
};

#endif

