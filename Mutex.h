#ifndef _MUTEX_H
#define _MUTEX_H

#include "pthread.h"

class Mutex {
	public:
		Mutex() { pthread_mutex_init(&mutex, 0); }
		~Mutex() { pthread_mutex_destroy(&mutex); }

		void Lock() { pthread_mutex_lock(&mutex); }
		void Unlock() { pthread_mutex_unlock(&mutex); }
	private:
		pthread_mutex_t mutex;
};

class ScopeLock {
	public:
		ScopeLock(Mutex &m) : mutex(m) { m.Lock(); }
		~ScopeLock() { mutex.Unlock(); }
	private:
		Mutex& mutex;
};


#endif

