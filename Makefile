CXX      := g++
CXXFLAGS := -Wall -Werror -O2 -g
LDFLAGS  := -lrt -lpthread

LIB_IN   := Transfer Command ClientHandler Address Socket FileSystem Thread Arguments Util Server Client
LIB_OBJ  := $(foreach F,$(LIB_IN),$(addsuffix .o,$(F)))
LIB_HDR  := $(foreach F,$(LIB_IN),$(addsuffix .h,$(F)))

TARGETS  := jftp jftpd
TARG_OBJ := $(foreach F,$(TARGETS),$(addsuffix .o,$(F)))

.PHONY: clean all

.DEFAULT: all

all: $(TARGETS) $(LIB_OBJ) $(TARG_OBJ)

# define a rule for making an o from a .cpp
%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< 

#define a rule for linking together OBJS into OUTPUT
jftp: jftp.o $(LIB_OBJ)
	$(CXX) $(LDFLAGS) $^ -o $@

jftpd: jftp.o $(LIB_OBJ)
	$(CXX) $(LDFLAGS) $^ -o $@

test:
	rm -rf lincoln.txt
	./jftpd &
	sleep 4
	./jftp 127.0.0.1 1234 -c "cd shared_files; get lincoln.txt; quit"
	diff lincoln.txt shared_files/lincoln.txt
	rm shared_files/lincoln.txt
	./jftp 127.0.0.1 1234 -c "cd shared_files; put lincoln.txt; quit"
	diff lincoln.txt shared_files/lincoln.txt
	rm lincoln.txt
	killall jftpd # end the server we forked

clean:
	rm -rf $(TARGETS) $(LIB_OBJ) $(TARG_OBJ)

