#include "ClientHandler.h"
#include "Util.h"
#include "FileSystem.h"
#include "Command.h"
#include "Transfer.h"

namespace {
	void ShowHelp(Socket s) {
		const string help = "Welcome to jftp! No one can help you now.\n"
		                    "quit    - disconnect from this prompt\n"
		                    "list,ls - list files in the current directory\n"
		                    "cd      - change the current directory\n";
		s.Send(help);
	}

	string ListFilesAsString(Path currentPath, string relative) {
		Path dir = currentPath.ChDir(relative);
		vector<Path> files = dir.List();
		stringstream resp;

		for(u32 i=0; i<files.size(); i++) {
			const Path &f = files[i];
			
			if(relative != "")
				resp << relative << "/";

			resp << f.Name() << (f.Directory() ? "/" : "") << "\n";
		}

		return resp.str();
	}

}

string ClientHandler::GetFile(string name, Path path) {
	stringstream resp;

	if(!path.Exists()) {
		resp << "Cannot get file \"" << name << "\" - it does not exist.\n";
		return resp.str();
	}

	FileLocker sflock(server, path);

	//--- try to lock the file so another thread doesn't nuke it
	if(!sflock.TryLock()) {
		resp << "Cannot lock file \"" << name << "\" - another transfer is using it.\n";
		return resp.str();
	}

	Socket destination = server->SpawnTransfer(socket);
	destination.ShouldClose();

	if(!ReadFileToSocket(path, destination)) {
		return "Error occured while transferring file.\n";
	}

	return "Transfer Complete.";
}

string ClientHandler::PutFile(string name, Path path) {
	stringstream resp;

	if(path.Exists()) {
		resp << "Cannot overwrite file \"" << name << "\" - it already exists.\n";
		return resp.str();
	}

	FileLocker sflock(server, path);

	//--- try to lock the file so another thread doesn't nuke it
	if(!sflock.TryLock()) {
		resp << "Cannot lock file \"" << name << "\" - another transfer is using it.\n";
		return resp.str();
	}

	Socket source = server->SpawnTransfer(socket);
	source.ShouldClose();

	if(!WriteFileFromSocket(path, source)) {
		return "Error occured while transferring file.\n";
	}

	return "Transfer Complete.";
}





ClientHandler::ClientHandler(Server *p, Socket s) {
	server = p;
	
	//--- take ownership of socket
	socket = s;
	socket.ShouldClose();
}

void ClientHandler::Respond(const string &resp) {
	if(socket) {
		socket.Send(resp);
	}
}

//--- returns false on quit
bool ClientHandler::DoCommand(const string &str) {
	Command cmd(str);
	const string &v = cmd.Verb();
	const string &arg = cmd.Noun();

	using namespace Cmd;

	if(v == Help) {
		Respond("Welcome to jftp! No one can help you now.\n"
		        "quit    - disconnect from this prompt\n"
		        "list,ls - list files in the current directory\n"
		        "cd      - change the current directory\n");
	} else if(v == Quit) {
		Respond("Bye!");
		socket.Close();
		return false;
	} else if(v == List) {
		Respond(ListFilesAsString(currentPath, arg));
	} else if(v == ChangeDir) {
		stringstream resp;
		Path newPath = currentPath + arg;
		
		if(newPath.Exists() && newPath.Directory()) {
			currentPath = newPath;
			Respond(currentPath+"\n");
		} else {
			Respond(stringf("Error: \"%s\" is not a valid directory or doesn't exist.\n", arg.c_str()));
		}
	} else if(v == CurrentDir) {
		Respond(string(currentPath) + "\n");
	} else if(v == Get) {
		Respond(GetFile(arg, currentPath + arg));
	} else if(v == Put) {
		Respond(PutFile(arg, currentPath + arg));
	}

	return true;
}

void ClientHandler::Process() {
	//--- welcome message
	u32 count = server->NumClients();
	string welcome;

	if(count <= 1) {
		welcome += "Welcome, you are the only client.\n";
	} else {
		welcome += stringf("Welcome, you are 1 of %d clients.\n", count);
	}
	socket.Send(welcome);

	while(Active() && socket) {
		string buf = socket.RecvAll();

		if(buf == "") {
			//no command yet
			continue;
		}
		
		bool shouldQuit = !DoCommand(buf);
		
		if(shouldQuit)
			break;
	}
}


