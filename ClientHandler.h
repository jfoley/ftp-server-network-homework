#ifndef _CLIENTHANDLER_H
#define _CLIENTHANDLER_H

#include "Server.h"

class ClientHandler : public Thread {
	public:
		ClientHandler(Server *p, Socket s);
		
		void Respond(const string &response);
		bool DoCommand(const string &cmd);
		virtual void Process();
	private:

		string GetFile(string name, Path path);
		string PutFile(string name, Path path);

		Server *server;
		Socket socket;
		Path   currentPath;
};



#endif

