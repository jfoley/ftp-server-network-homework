#ifndef _CLIENT_H
#define _CLIENT_H

#include "Command.h"

class Client {
	public:
		Client(string hostName, u16 port);
		void SendCommand(string command);
		string HandleResponse(Command cmd, string response);
		bool HasQuit() const { return quit; }
	private:
		bool quit;
		Socket socket;
};

#endif

