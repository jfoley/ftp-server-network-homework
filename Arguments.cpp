#include "Arguments.h"
#include "Util.h"
#include <cassert>
#include <cstdarg>
#include <algorithm>
	
#define checkChar(c) \
		assert(c >= ' ' && c != ArgumentMarker && c < '~')

namespace {
	const char ArgumentMarker   = '-';
	const char AssignmentMarker = '=';
	const char PathSeparator    = '/';

	bool isArgument(const string &s) {
		return s.size() > 1 && s[0] == ArgumentMarker;
	}
	bool isArgumentEquals(const string &s) {
		return s.size() >= 4 && s[2] == AssignmentMarker;
	}
}

Arguments::Arguments(string name) {
	parsed = false;
	progName = name;
	while(contains(progName, PathSeparator)) {
		progName = splitAfter(progName, PathSeparator);
	}
}

void Arguments::DefineFlag(char c, string desc) {
	checkChar(c);

	ArgDef flag;
	flag.c    = c;
	flag.desc = desc;
	flag.type = Flag;

	argdefs.push_back(flag);
}

void Arguments::DefineOption(char c, string desc) {
	checkChar(c);
	
	ArgDef opt;
	opt.c    = c;
	opt.desc = desc;
	opt.type  = Option;

	argdefs.push_back(opt);
}

void Arguments::DefineParam(char c, string desc) {
	checkChar(c);
	
	ArgDef p;
	p.c = c;
	p.desc = desc;
	p.type = Parameter;

	argdefs.push_back(p);
}

bool Arguments::Parse(int argc, char **argv) {
	
	//--- create a stack of parameters to find
	vector<char> parameters;
	for(u32 i=0; i<argdefs.size(); i++) {
		if(argdefs[i].type == Parameter)
			parameters.push_back(argdefs[i].c);
	}
	std::reverse(parameters.begin(), parameters.end());

	// start at one to skip program name
	for(int i=1; i<argc; i++) {
		string cur(argv[i]);

		if(!isArgument(cur)) {
			if(!parameters.size()) {
				Errorf("Invalid argument \"%s\"", cur.c_str());
				return false;
			}
			//--- take parameter
			char curParam = parameters.back();
			results[curParam] = cur;
			parameters.pop_back();
			continue;
		}

		int allowed = -1;
		for(size_t j=0; j<argdefs.size(); j++) {
			if(cur[1] == argdefs[j].c) {
				allowed = j;
				break;
			}
		}
		if(allowed == -1) {
			Errorf("Unexpected argument \"%s\"", cur.c_str());
			return false;
		}
		const ArgDef &def = argdefs[allowed];

		string value = "";

		//--- handle cases where argument is of type -x=something
		if(isArgumentEquals(cur)) {
			if(!def.type == Flag) {
				Errorf("Provided parameter for \"%c%c\", but none expected.", ArgumentMarker, def.c);
				return false;
			}
			value = splitAfter(cur, AssignmentMarker);

		//--- handle cases where argument is of type -x
		} else if(cur.size() == 2) {

			//--- pull in next argument if available and not obviously a -something
			if(def.type == Option) {
				if(i+1 == argc || argv[i+1][0] == ArgumentMarker) {
					Errorf("Expected parameter after \"%s\", but none found.", cur.c_str());
					return false;
				}
				value = argv[++i];
			} else {
				value = "true";
			}
		}

		assert(value != "");

#if DEBUG_ARGS
		cout << "parse " << ArgumentMarker << def.c << AssignmentMarker << value << "\n";
#endif

		if(GetOption(def.c, "") != "") {
			Errorf("Redefinition of \"%c%c\" with \"%s\"", ArgumentMarker, def.c, cur.c_str());
			return false;
		}
		results[def.c] = value;
	}
	
	//--- should have been popped all the way to nothing if parameters present
	if(parameters.size() != 0) {
		cerr << "Expected " << parameters.size() << " more parameters!\n";
		return false;
	}

	return true;
}

void Arguments::Print() const {
	cout << "Usage: " << progName << " ";
	
	u32 count = 0;
	for(u32 i=0; i<argdefs.size(); i++) {
		const ArgDef &def = argdefs[i];
		if(def.type == Parameter) {
			cout << def.desc << " ";
			count++;
		}
	}

	if(count != argdefs.size())
		cout << " [options]\n";
	else
		cout << "\n\n";

	for(size_t i=0; i<argdefs.size(); i++) {
		const ArgDef &def = argdefs[i];
		if(def.type != Parameter) {
			cout << "  " << ArgumentMarker << def.c << "\n     " << def.desc << "\n\n";
		}
	}
}

void Arguments::Errorf(const char *fmt, ...) {
	FILE* ERR = stderr;

	fprintf(ERR, "%s: ", progName.c_str());

	va_list args;
	va_start(args, fmt);
	vfprintf(ERR, fmt, args);
	va_end(args);

	fprintf(ERR, "\n\n");
}

bool Arguments::GetFlag(char c) const {
	return (results.find(c) != results.end());
}

string Arguments::GetParam(char c) const {
	map<char, string>::const_iterator it = results.find(c);
	if(it == results.end()) {
		return "";
	}

	return it->second;
}

string Arguments::GetOption(char c, string fallback) const {
	map<char, string>::const_iterator it = results.find(c);
	if(it == results.end()) {
		return fallback;
	}

	return it->second;
}

