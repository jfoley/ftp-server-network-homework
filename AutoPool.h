#ifndef _AUTOPOOL_H
#define _AUTOPOOL_H

#include "Mutex.h"
#include "types.h"

//--- a set of items that implement a " volatile bool Active() const "
//    when tehy return false, they are deleted and removed.

template <class T>
class AutoPool {
	public:
		~AutoPool() { Clear(); }
		void Clear() {
			while(refs.size()) {
				if(refs.back()->Active())
					delete refs.back();
				refs.pop_back();
			}
		}
		u32 Count() {
			u32 count = 0;
			ScopeLock lock(mutex);
			for(u32 i=0; i<refs.size(); i++) {
				if(refs[i] && refs[i]->Active())
					count++;
			}
			return count;
		}
		void Collect() {
			u32 count = 0;

			ScopeLock lock(mutex);
			for(u32 i=0; i<refs.size(); i++) {
				if(refs[i] && !refs[i]->Active()) {
					delete refs[i];
					refs[i] = 0;
				}
				count ++;
			}
		}
		void Insert(T ptr) {
			ScopeLock lock(mutex);
			for(u32 i=0; i<refs.size(); i++) {
				if(!refs[i]) {
					refs[i] = ptr;
					return;
				}
			}
			refs.push_back(ptr);
		};


	private:
		Mutex mutex;
		vector<T> refs;
};


#endif

