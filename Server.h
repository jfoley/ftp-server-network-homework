#ifndef _SERVER_H
#define _SERVER_H

#include "jftp.h"
#include "Thread.h"
#include "AutoPool.h"
#include "Socket.h"
#include <set>
using std::set;

class Server {
	public:
		u32 NumClients() { return clients.Count(); }

		void NewClient(Socket s);

		Socket SpawnTransfer(Socket client);

		//--- file operations
		//    Read & Write return true if able to lock file
		bool TryLockFile(const string &fileName);
		void DoneFile(const string &fileName);

	private:
		Mutex mutex;
		set<string> lockedFiles;
		AutoPool<Thread*> clients;
};

struct FileLocker {
	FileLocker(Server* s, string p) : server(s), path(p) {}
	~FileLocker() { server->DoneFile(path); }
	bool TryLock() { return server->TryLockFile(path); }
	Server *server;
	string path;
};



#endif

