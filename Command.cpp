#include "Command.h"
#include "Util.h"

Command::Command(string input) {
	string s = toLower(compactSpaces(input));

	string v; //original verb
	
	if(contains(s, ' ')) {
		v = splitBefore(s, ' ');
		noun = splitAfter(s, ' ');
	} else {
		v = s;
	}

	using namespace Cmd;

	//--- be awesome if C++ supported switch statements with strings
	if(v == "help" || v == "h" || v == "idk" || v == "?") {
		verb = Help;

	} else if(v == "exit" || v == "quit" || v == "bye" || v == "flee") {
		verb = Quit;
	} else if(v == "get" || v == "fetch" || v == "download" || v == "yank") {
		verb = Get;
	} else if(v == "put" || v == "upload") {
		verb = Put;
	} else if(v == "list" || v == "show" || v == "disp" || v == "ls") {
		verb = List;
	} else if(v == "cd" || v == "changedir") {
		verb = ChangeDir;
	} else if(v == "here" || v == "pwd" || v == "cwd") {
		verb = CurrentDir;
	} else {
		verb = "";
	}

	if(v == Get || v == Put || v == ChangeDir) {
		if(noun == "") {
			cout << "Expected argument for " << v << "\n";
		}
	}

}

bool Command::operator==(const string &other) const {
	return verb == compactSpaces(other);
}



