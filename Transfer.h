#ifndef _TRANSFER_H
#define _TRANSFER_H

#include "FileSystem.h"
#include "Socket.h"

bool ReadFileToSocket(string fileName, Socket destination);
bool WriteFileFromSocket(string fileName, Socket source);

#endif

