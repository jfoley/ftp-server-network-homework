
#include "Transfer.h"
#include "jftp.h"
#include <fstream>
using std::fstream;

bool ReadFileToSocket(string fileName, Socket destination) {
	if(!destination)
		return false;

	fstream file;
	file.open(fileName.c_str(), fstream::in | fstream::binary);
	
	if(file.bad()) {
		return false;
	}

	while(file.good()) {
		string buf;
		buf.resize(BufferSize);

		file.read(&buf[0], buf.size());
		u32 count = file.gcount();

		buf.resize(count);

		destination.Send(stringf("%u ", count));
		destination.Send(buf);
	}
	file.close();

	//--- terminate with a packet of zero length
	destination.Send("0\n");
	
	cout << "Transfer complete!\n";

	return true;
}

bool WriteFileFromSocket(string fileName, Socket source) {
	if(!source)
		return false;

	fstream file;
	file.open(fileName.c_str(), fstream::out | fstream::binary | fstream::trunc);

	//--- while it keeps appearing with data
	while(DataWithinTimeout(source)) {
		string length = source.RecvUntil(' ');
		u32 count = toInt(length, 0);

		//--- we end with a chunk of size 0
		if(count == 0)
			break;

		string chunk;
		chunk.resize(count);

		//--- read the chunk from the socket and stick it in the file
		source.Recv(&chunk[0], chunk.size());
		file.write(&chunk[0], chunk.size());
	}

	cout << "Transfer complete!\n";

	file.close();
	return true;
}

