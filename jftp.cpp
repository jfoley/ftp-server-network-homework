#include "jftp.h"
#include "Client.h"
#include "Command.h"
#include "Transfer.h"

namespace {
	const string Prompt = "jftp> ";
}

Arguments do_args(int argc, char **argv) {
	Arguments args(argv[0]);
	args.DefineParam(ARG_DEST, "hostname");
	args.DefineParam(ARG_PORT, "port");
	args.DefineOption(ARG_CMDS, "A set of commands to run at the prompt");

	if(!args.Parse(argc, argv)) {
		args.Print();
		exit(-1);
	}
	if(args.GetFlag(ARG_HELP)) {
		args.Print();
		exit(0);
	}

	return args;
}

int main(int argc, char **argv) {
	Arguments args = do_args(argc, argv);

	string hostName = args.GetParam(ARG_DEST);
	u16 port = toInt(args.GetParam(ARG_PORT), DefaultPort);

	Client client(hostName, port);
	if(client.HasQuit())
		return -1;

	vector<string> cmds = split(args.GetOption(ARG_CMDS, ""), ';');
	for(u32 i=0; i<cmds.size() && !client.HasQuit(); i++) {
		if(isSpaces(cmds[i]))
			continue;

		client.SendCommand(cmds[i]);
	}

	while(!client.HasQuit()) {
		cout << Prompt; cout.flush();

		string input;
		if(!getline(cin, input))
			break;

		client.SendCommand(input);
	}

	cout << "\n";
	return 0;
}


