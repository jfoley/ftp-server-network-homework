#include "Util.h"
#include <cstdarg>
#include <cstdio>

string stringf(const char *fmt, ...) {
	string buf;
	int len = 0;
	va_list args;

	va_start(args, fmt);
	len = vsnprintf(0, 0, fmt, args)+1;
	va_end(args);

	buf.resize(len);

	va_start(args, fmt);
	vsnprintf(&buf[0], buf.size(), fmt, args);
	va_end(args);

	buf.resize(len-1); // chop null

	return buf;
}

vector<string> split(const string &src, const char delimiter) {
	vector<string> result;
	string cur = src;
	
	while(contains(cur, delimiter)) {
		size_t pos = cur.find(delimiter);
		result.push_back(cur.substr(0,pos));
		cur = cur.substr(pos+1);
	}

	result.push_back(cur);
	return result;
}

string join(const vector<string> &src, const char delimiter) {
	string result;

	for(size_t i=0; i<src.size(); i++) {
		if(i != 0) {
			result += delimiter;
		}
		result += src[i];
	}

	return result;
}


string splitAfter(const string &src, const char delimiter) {
	size_t pos = src.find(delimiter);
	return (pos == string::npos) ? src : src.substr(pos+1);
}

string splitAfterLast(const string &src, const char delimiter) {
	size_t pos = src.rfind(delimiter);
	return (pos == string::npos) ? src : src.substr(pos+1);
}

string splitBeforeLast(const string &src, const char delimiter) {
	size_t pos = src.rfind(delimiter);
	return (pos == string::npos) ? src : src.substr(0,pos);
}

string splitBefore(const string &src, const char delimiter) {
	size_t pos = src.find(delimiter);
	return (pos == string::npos) ? src : src.substr(0,pos);
}

bool contains(const string &haystack, const char needle) {
	return haystack.find(needle) != string::npos;
}

string toLower(const string &mixed) {
	string result(mixed);
	
	for(size_t i=0; i<result.size(); i++)
		result[i] = tolower(result[i]);

	return result;
}

bool isSpaces(const string &s) {
	for(size_t i=0; i<s.size(); i++)
		if(s[i] > ' ')
			return false;
	return true;
}

string compactSpaces(const string &s) {
	string result;

	if(isSpaces(s)) {
		return "";
	}

	int lastChar = -1;
	for(size_t i=0; i<s.length(); i++, lastChar++) {
		const char &c = s[i];

		if(lastChar == -1 || s[lastChar] <= ' ') {
			if(c <= ' ') {
				continue;
			} else {
				result.push_back(c);
			}
		} else {
			result.push_back(c);
		}
	}

	//--- delete last space if any
	if(s[lastChar] <= ' ' && result.size()) {
		result.resize(result.size()-1);
	}

	return string(result.c_str());
}

bool startsWith(const string& source, const string &prefix) {
	if(prefix.size() > source.size()) {
		return false;
	} else if(prefix.size() == source.size()) {
		return prefix == source;
	}

	for(size_t i=0; i<prefix.size(); i++)
		if(source[i] != prefix[i]) return false;

	return true;
}

char reverseElement(const string &src, size_t i) {
	return src[ src.size() - i - 1];
}

bool endsWith(const string &source, const string &suffix) {
	if(suffix.size() > source.size())
		return false;
	else if(suffix.size() == source.size())
		return suffix == source;

	for(size_t i=0; i<=suffix.size(); i--) {
		if(reverseElement(suffix, i) != reverseElement(source, i))
			return false;
	}

	return true;
}

int toInt(const string &n, int fallback) {
	string s = toLower(compactSpaces(n));

	if(isSpaces(s))
		return fallback;

	bool negative = false;

	//--- determine base from hints
	int base = 10;
	if(startsWith(s, "0x")) {
		base = 16;
		s = s.substr(2);
	} else if(startsWith(s, "0b")) {
		base = 2;
		s = s.substr(2);
	} else if(startsWith(s, "0")) {
		base = 8;
		s = s.substr(1);
	}

	//--- handle negatives
	if(base == 10 && startsWith(s, "-")) {
		negative = true;
		s = s.substr(1);
	}
	
	const string hex = "0123456789abcdef";
	int value = 0;

	for(size_t i=0; i<s.size(); i++) {
		int offset = (int) hex.find(s[i]);
		
		//--- not found or too big
		if(offset == (int)(string::npos) || offset > base) {
			return fallback;
		} else {
			value *= base;
			value += offset;
		}
	}
	
	//--- set sign and be done
	if(negative) value *= -1;
	return value;
}


