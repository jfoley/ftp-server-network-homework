#include "jftp.h"
#include "Client.h"
#include "Command.h"
#include "Transfer.h"

Client::Client(string hostName, u16 port) {
	quit = true;

	Address server = Address::Resolve(hostName, port);
	if(!server) {
		cerr << "Address " << hostName << ":" << port << " could not be resolved. Are you connected to the internet?\n";
		return;
	}

	if(!socket.OpenStreamClient(server)) {
		cerr << "Could not connect to " << server << "...\n";
		return;
	}

	//--- setup correctly
	quit = false;

	//--- check for welcome message
	if(socket.HasData(TimeoutSeconds)) {
		string welcome = socket.RecvAll();
		cout << welcome;
	}
}

void Client::SendCommand(string command) {
	Command cmd(command);
	
	if(!cmd.Valid()) {
		cout << "Client won't send invalid command \"" << command << "\"\n";
		return;
	}
	
	socket.Send(string(cmd));

	if(cmd == Cmd::Quit) {
		quit = true;
		return;
	} else if(cmd == Cmd::Get) {
		string fileName = cmd.Noun();
		Path fileObj = Path(fileName);

		if(fileObj.Exists()) {
			cout << "Local file \"" << fileName << "\" already exists.\n";
			return;
		}
	} else if(cmd == Cmd::Put) {
		string fileName = cmd.Noun();
		Path fileObj = Path(fileName);

		if(!fileObj.Exists()) {
			cout << "Local file \"" << fileName << "\" does not exist; nothing to transfer.\n";
			return;
		}
	}
	
	bool hasResponse = false;
	for(u32 trial=0; trial < RetryCount; trial++) {
		if(socket.HasData(TimeoutSeconds))
			hasResponse = true;
	}

	if(!hasResponse) {
		cout << "Server Timed Out.\n";
		quit = true;
		return;
	}

	cout << HandleResponse(cmd, socket.RecvAll());
}

string Client::HandleResponse(Command cmd, string resp) {
	stringstream out;
	string first = splitBefore(resp, ' ');
	
	if(first == NewTransferMsg) {
		Socket transfer;
		u16 newPort = toInt(splitAfter(resp, ' '), 0);

		if(!newPort) {
			quit = true;
			return "Server gave us back a bad response!\n";
		}

		//show(newPort);

		//--- create an address from old host, new port
		Address target = Address::Resolve(socket.GetAddress().Host(), newPort);
		transfer.OpenStreamClient(target);
		transfer.ShouldClose();

		if(!transfer) {
			out << "Couldn't establish a connection to transfer socket: " << transfer << " @ " << target << "\n";
			return out.str();
		}

		if(cmd == Cmd::Get) {
			if(!WriteFileFromSocket(cmd.Noun(), transfer)) {
				return "An error occurred while transferring file.\n";
			}
		} else if(cmd == Cmd::Put) {
			if(!ReadFileToSocket(cmd.Noun(), transfer)) {
				return "An error occurred while transferring file.\n";
			}
		} else {
			assert(0);
		}
	} else {
		out << resp;
	}

	return out.str();
}


